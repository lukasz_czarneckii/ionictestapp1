angular.module('starter')

.factory('DataMapper', ['$q', '$http', 'ApiEndpoint', function($q, $http, ApiEndpoint) {

  var _dataUrl = ApiEndpoint.url + '/data.json',
      _artists = [],
      _albums = [];

  function Artist(options) {
    this.id = options.id;
    this.name = options.name;
    this.description = options.description;
    this.genres = options.genres;
    this.picture = options.picture;

    this.albums = options.albums || [];
  }

  function Album(options) {
    this.id = options.id;
    this.artistId = options.artistId;
    this.title = options.title;
    this.picture = options.picture;
  }

  function _getData() {
    return $http.get(_dataUrl)
    .then(function success(response) {
      var rawData = response.data;
      _artists = [];
      _albums = [];

      for(var i = 0; i < rawData.albums.length; ++i) {
        var rawAlbum = rawData.albums[i];

        _albums.push(new Album({
          id: rawAlbum.id,
          artistId: rawAlbum.artist_id,
          title: rawAlbum.title,
          picture: rawAlbum.picture
        }));
      }

      for(var i = 0; i < rawData.artists.length; ++i) {
        var rawArtist = rawData.artists[i];

        _artists.push(new Artist({
          id: rawArtist.id,
          name: rawArtist.name,
          description: rawArtist.description,
          genres: rawArtist.genres,
          picture: rawArtist.picture,
          albums: _albums.filter(function(album) { return album.artistId == rawArtist.id; })
        }));
      }
    }, function error(response) {
      console.log(response);
    });
  }

  function getArtists() {
    return _getData()
    .then(function() { return _artists; });
  }

  function getArtist(id) {
    return _getData()
    .then(function() {
      for(var i = 0; i < _artists.length; ++i) {
        if(_artists[i].id === id) {
          return _artists[i];
        }
      }
    });
  }

  function getAlbums() {
    return _getData()
    .then(function() { return _albums; });
  }

  function getAlbum(id) {
    return _getData()
    .then(function() {
      for(var i = 0; i < _albums.length; ++i) {
        if(_albums[i].id === id) {
          return _albums[i];
        }
      }
    });
  }

  return {
    getArtists: getArtists,
    getArtist: getArtist,
    getAlbums: getAlbums,
    getAlbum: getAlbum,
  };
}]);

