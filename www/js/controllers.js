angular.module('starter')

.controller('AppController', ['$scope', '$rootScope', '$state', '$ionicNavBarDelegate', '$ionicPopup', '$cordovaNetwork', function($scope, $rootScope, $state, $ionicNavBarDelegate, $ionicPopup, $cordovaNetwork) {
  var isIOS = ionic.Platform.isIOS();
  $ionicNavBarDelegate.showBackButton(isIOS);

  document.addEventListener('deviceready', function() {
    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
      $ionicPopup.alert({
        title: "Network is unavailable",
      });
    });
  }, false);
}])

.controller('ArtistsController', ['$scope', 'DataMapper', function($scope, DataMapper) {
  DataMapper.getArtists()
  .then(function(artists) {
    $scope.artists = artists;
  });
}])

.controller('ArtistDetailsController', ['$scope', '$stateParams', 'DataMapper', function($scope, $stateParams, DataMapper) {
  DataMapper.getArtist(+$stateParams.artistId)
  .then(function(artist) {
    $scope.artist = artist;
  });
}]);

